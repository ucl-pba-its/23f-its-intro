---
hide:
  - footer
  - toc
---

# UCL PBa IT Sikkerhed - Introduktion til IT sikkerhed

På dette website finder du ugeplaner, dokumenter og links til brug i faget 

## Introduktion til IT sikkerhed Forår 2023  

Formålet med faget er at få alle studerende op på et fælles grundniveau.
Elementet omhandler grundlæggende programmering og netværk med henblik på it-sikkerhed.  
Den studerende bliver introduceret til programmering med et programmeringssprog, der normalt anvendes indenfor sikkerhedsverdenen.  
Den studerende skal have viden, færdigheder og kompetencer i centrale begreber i forhold til it-sikkerhed, i forhold til netværk (grundlæggende begreber som trafik monitorering ved sniffing). Yderligere kigges der på sikkerhedsaspekter ved protokollerne.

De studerende vil have forskellige baggrund og dermed kompentencer når de starter. Dette vil vi bruge til at få de studerende til at hjælpe hinanden og på den blive skarpe på deres egne styrker og svagheder.

Faget er på 5 ECTS point.

### Studieordning

Studieordningen er i 2 dele, en national del og en institutionel del.  

Begge dele kan findes i studiedokumenter på ucl.dk [https://www.ucl.dk/studiedokumenter/it-sikkerhed](https://www.ucl.dk/studiedokumenter/it-sikkerhed)

## Læringsmål fra studie ordningen

### Viden

Den studerende har viden om og forståelse for:  

- Grundlæggende programmeringsprincipper
- Grundlæggende netværksprotokoller
- Sikkerhedsniveau i de mest anvendte netværksprotokoller

### Færdigheder

Den studerende kan supportere løsning af sikkerhedsarbejde ved at:  

- Anvende primitive datatyper og abstrakte datatyper
- Konstruere simple programmer der bruge SQL databaser
- Konstruere simple programmer der kan bruge netværk
- Konstruere og anvende tools til f.eks. at opsnappe samt filtrere netværkstrafik
- Opsætte et simpelt netværk.
- Mestre forskellige netværksanalyse tools
- Læse andres scripts samt gennemskue og ændre i dem

### Kompetencer

Den studerende kan:   

- Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv
