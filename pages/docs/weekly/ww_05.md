---
Week: 10
Content: xx, xx, xx, xx
Material: See links in weekly plan
Initials: NISI
---

# Uge 10 - Python programmering

Programmering er noget i allesammen har stødt på før i forbindelse med grunduddannelsen.
Arbejdet med sikkerhed kommer ofte i berøring med programmering, det kan være du i din karriere skal arbejde med at udvikle sikker software, eller samarbejde med udviklere, det kan være du skal evaluere scripts til netværks automation eller du skal analysere en suspekt fil som en medarbejder har modtaget på mail. Scripts bruges ofte i virksomheder til at automatisere forskellige processer, her er det også en fordel at kunne programmere.

Denne uges står i programmeringens tegn og i skal arbejde med både analyse og udvikling af scripts i Python.

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Forberedelse

- Læs undervisningsplanen og opgaver
- Læs kapitel 5 – Udvikling af sikker software, i "IT-Sikkerhed i praksis".
- Hvis du ikke har brugt Python før, eller har brug for en genopfrisker så brug lidt tid på denne [Python tutorial](https://www.w3schools.com/python/)

### Praktiske mål

- Opgaver fuldført

### Opgaver

1. [THM: Python basics](https://tryhackme.com/room/pythonbasics)
2. [Opgave 8 - Python recap](https://ucl-pba-its.gitlab.io/exercises/intro/8_intro_opgave_python_recap/)

### Læringsmål

- Grundlæggende programmeringsprincipper
- Anvende primitive og abstrakte datatyper
- Læse andres scripts samt gennemskue og ændre i dem

## Afleveringer

- Opgaver dokumenteret på gitlab pages

## Skema

Herunder er det planlagte program for ugen.

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 8:30  | Opgaver fra sidst      |
| 9:30  | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer og links



